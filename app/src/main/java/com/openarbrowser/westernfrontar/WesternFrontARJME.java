/* WesternFrontARJME
 *
 * Combines and expands on the LocationAccessJME and SensorFusionJME projects from Chapter 4.
 *
 * Jens Grubert, Raphael Grasset "Augmented Reality for Android Application Development", Packt Publishing, 2013.
 *
 * Copyright © 2013  / Packt Publishing.
 *
 */

package com.openarbrowser.westernfrontar;

import android.location.Location;
import android.util.Log;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetNotFoundException;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Image;
import com.jme3.texture.Texture2D;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class WesternFrontARJME extends SimpleApplication  implements AnimEventListener {

    private static final String TAG = "WesternFrontARJME";
    // The geometry which will represent the video background
    private Geometry mVideoBGGeom;
    // The material which will be applied to the video background geometry.
    private Material mvideoBGMat;
    // The texture displaying the Android camera preview frames.
    private Texture2D mCameraTexture;
    // the JME image which serves as intermediate storage place for the Android
    // camera frame before the pixels get uploaded into the texture.
    private Image mCameraImage;
    // A flag indicating if the scene has been already initialized.
    private boolean mSceneInitialized = false;
    // A flag indicating if a new Android camera image is available.
    boolean mNewCameraFrameAvailable = false;
    // A flag to indicate if the location has been updated once
    static boolean firstTimeLocation=true;

    //the User position which serves as intermediate storage place for the Android
    //Location listener position update
    private Vector3f mUserPosition;

    //A flag indicating if a new Location is available
    private boolean mNewUserPositionAvailable =false;



    private Quaternion mInitialCamRotation;
    private Quaternion mRotXQ;
    private Quaternion mRotYQ;
    private Quaternion mRotZQ;
    private Quaternion mRotXYZQ;

    //the User rotation which serves as intermediate storage place for the Android
    //Sensor listener motion update
    private Quaternion mCurrentCamRotationFused;
    private Quaternion mCurrentCamRotation;

    //A flag indicating if a new Rotation is available
    private boolean mNewUserRotationFusedAvailable =false;

    Camera fgCam;
    private float mForegroundCamFOVY = 50; // for a Samsung Galaxy SII


    private Vector3f mNinjaPosition;
    Location locationNinja;
    Spatial ninja;

    // for animation
    // The controller allows access to the animation sequences of the model
    private AnimControl mAniControl;
    // the channel is used to run one animation sequence at a time
    private AnimChannel mAniChannel;

    static int counterTime=1000;

    //Activity POI List holding query results
    private List<POI> mPOIListActivity;
    //JME POI List holding JME objects
    private List<POI> mPOIListJME;
    //A semaphore to prevent deadlock when synchronising arrays
    private Boolean mSemaphore = true;

    ViewPort fgVP;
    ViewPort videoBGVP;

    public static void main(String[] args) {
        WesternFrontARJME app = new WesternFrontARJME();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        // Do not display statistics
        setDisplayStatView(false);
        setDisplayFps(false);

        mPOIListActivity = new LinkedList<>();
        mPOIListJME = new LinkedList<>();;

        // we use our custom viewports - so the main viewport does not need the  rootNode
        viewPort.detachScene(rootNode);
        initVideoBackground(settings.getWidth(), settings.getHeight());
        //initForegroundScene may be removed as a location is required
        //before loading POI objects which may not be available.
        initForegroundScene();
        initBackgroundCamera();
        initForegroundCamera(mForegroundCamFOVY);

        mSceneInitialized = true;
    }

    @Override
    public void stop() {
        super.stop();
        fgVP.setEnabled(false);
        videoBGVP.setEnabled(false);

    }

    /**
     * This function creates the geometry, the viewport and the virtual camera
     * needed for rendering the incoming Android camera frames in the scene
     * graph
     * @param screenWidth
     *                  Width of the screen
     * @param screenHeight
     *                  Height of the screen
     */
    public void initVideoBackground(int screenWidth, int screenHeight) {
        // Create a Quad shape.
        Quad videoBGQuad = new Quad(1, 1, true);
        // Create a Geometry with the Quad shape
        mVideoBGGeom = new Geometry("quad", videoBGQuad);
        float newWidth = 1.f * screenWidth / screenHeight;
        // Center the Geometry in the middle of the screen.
        mVideoBGGeom.setLocalTranslation(-0.5f * newWidth, -0.5f, 0.f);//
        // Scale (stretch) the width of the Geometry to cover the whole screen
        // width.
        mVideoBGGeom.setLocalScale(1.f * newWidth, 1.f, 1);
        // Apply a unshaded material which we will use for texturing.
        mvideoBGMat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        mVideoBGGeom.setMaterial(mvideoBGMat);
        // Create a new texture which will hold the Android camera preview frame
        // pixels.
        mCameraTexture = new Texture2D();
        mUserPosition=new Vector3f();
        mNinjaPosition=new Vector3f();
        locationNinja=new Location("ninjaPOI");
        //mSceneInitialized = true;


        mCurrentCamRotationFused=new Quaternion(0.f,0.f,0.f,1.f);
        mCurrentCamRotation = new Quaternion(0.f,0.f,0.f,1.f);
    }

    /**
     * Initialises background camera, viewport and geometry.
     */
    public void initBackgroundCamera() {
        // Create a custom virtual camera with orthographic projection
        Camera videoBGCam = cam.clone();
        videoBGCam.setParallelProjection(true);
        // Also create a custom viewport.
        videoBGVP = renderManager.createMainView("VideoBGView",
                videoBGCam);
        // Attach the geometry representing the video background to the
        // viewport.
        videoBGVP.attachScene(mVideoBGGeom);
    }

    /**
     * Initialises ninja. Called on application startup by simpleInitApp.
     * May be removed as POI objects cannot be loaded until location data
     * is available.
     */
    public void initForegroundScene() {
        // Load a model from test_data (OgreXML + material + texture)
        ninja = assetManager.loadModel("Models/Ninja/Ninja.mesh.xml");
        ninja.scale(0.05f, 0.05f, 0.05f);
        ninja.rotate(0.0f, -3.0f, 0.0f);
        ninja.setLocalTranslation(0.0f, -2.5f, -50.0f);
        rootNode.attachChild(ninja);

        // You must add a light to make the model visible
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.1f, -0.7f, -1.0f));
        rootNode.addLight(sun);

        mAniControl = ninja.getControl(AnimControl.class);
        mAniControl.addListener(this);
        mAniChannel = mAniControl.createChannel();
        // show animation from beginning
        mAniChannel.setAnim("Walk");
        mAniChannel.setLoopMode(LoopMode.Loop);
        mAniChannel.setSpeed(1f);
    }

    /**
     * Initialises the camera used to render foreground objects, including point of interest
     * @param fovY
     *            Camera field of view
     */
    public void initForegroundCamera(float fovY) {

        fgCam = new Camera(settings.getWidth(), settings.getHeight());
        fgCam.setLocation(new Vector3f(0f, 0f, 0f));
        fgCam.setAxes(new Vector3f(-1f, 0f, 0f), new Vector3f(0f, 1f, 0f), new Vector3f(0f, 0f, -1f));

        mInitialCamRotation = new Quaternion();
        mInitialCamRotation.fromAxes(new Vector3f(-1f, 0f, 0f), new Vector3f(0f, 1f, 0f), new Vector3f(0f, 0f, -1f));

        mRotXQ = new Quaternion();
        mRotYQ = new Quaternion();
        mRotZQ = new Quaternion();
        mRotXYZQ = new Quaternion();

        fgCam.setFrustumPerspective(fovY, settings.getWidth() / settings.getHeight(), 0.5f, 50000);
        fgVP = renderManager.createMainView("ForegroundView", fgCam);
        fgVP.attachScene(rootNode);
        fgVP.setClearFlags(false, true, false);
        fgVP.setBackgroundColor(ColorRGBA.Blue);
    }

    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
        // unused
    }

    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
        // unused
    }

    /**
    * This method retrieves the preview images from the Android world and puts
    * them into a JME image.
     */
    public void setVideoBGTexture(final Image image) {
        if (!mSceneInitialized) {
            return;
        }
        mCameraImage = image;
        mNewCameraFrameAvailable = true;
    }

    /**
     * Sets new rotation from fused sensor data.
     *
     * @param pitch
     *              Pitch
     * @param roll
     *              Roll
     * @param heading
     *              Heading
     */
    public void setRotationFused(float pitch, float roll, float heading) {
        if (!mSceneInitialized) {
            return;
        }
        //		pitch: cams x axis roll: cams y axisheading: cams z axis
        mRotXYZQ.fromAngles(pitch + FastMath.HALF_PI, roll - FastMath.HALF_PI, 0);
        mCurrentCamRotationFused = mInitialCamRotation.mult(mRotXYZQ);
        mNewUserRotationFusedAvailable =true;
    }

    /**
     * Sets the user's position and repositions points of interest within
     * the ENU coordinate system.
     * @param location
     *                  User's lat/long position.
     */
    public void setUserLocation(Location location) {

        if (!mSceneInitialized) {
            return;
        }
        Log.d(TAG,"update camera position");

        //Update POI list.
        mPOIListJME = updatePOIList(mPOIListActivity);

        WSG84toECEF(location,mUserPosition);

        //Ninja specific
        //Set-up user location vs ninja
        if (firstTimeLocation) {
            //put it at 10 meters
            locationNinja.setLatitude(location.getLatitude()+0.0003);
            locationNinja.setLongitude(location.getLongitude());
            firstTimeLocation=false;
        }

        Vector3f ECEFNinja=new Vector3f();
        Vector3f ENUNinja=new Vector3f();

        WSG84toECEF(locationNinja,ECEFNinja);

        ECEFtoENU(locationNinja,mUserPosition,ECEFNinja,ENUNinja);
        //x=east,y=north,z=up
        mNinjaPosition.set(ENUNinja.x, 0, ENUNinja.y);
        Log.d(TAG, "Ninja Pos=" + mNinjaPosition.toString());

        //POI List specific
        //Set-up user location vs mPOIListJME
        Vector3f ECEFpoi=new Vector3f();
        Vector3f ENUpoi=new Vector3f();

        //Semaphore restricts access to mPOIListJME
        //For each POI convert its location to ECEF
        for(POI poi: mPOIListJME){
            WSG84toECEF(poi.gpsLocation, ECEFpoi);
            //Convert ECEF to ENU
            ECEFtoENU(poi.gpsLocation,mUserPosition,ECEFpoi,ENUpoi);
            //Set virtual location
            poi.enuLocation.set(ENUpoi.x, 0, ENUpoi.y);
            Log.d(TAG, "POI Pos=" + poi.enuLocation.toString());
        }
        mNewUserPositionAvailable =true;
        //Control flow continues in simpleUpdate
    }

    /**
     * Assigns an updated POI List to mPOIListActivity
     * @param poiList
     *              List of POI objects.
     */
    public void setPOIList(List<POI> poiList) {
        //Semaphore prevents setUserLocation from writing to mPOIListJME
        mPOIListActivity = poiList;
        //Control flow continues in setUserLocation
    }

    /**
     * Updates POI List
     */
    private List<POI> updatePOIList(List<POI> reference){

        List<POI> l2 = new LinkedList<>(reference);

        Iterator<POI> itMaster = mPOIListJME.iterator();

        //Synchronises the JME and Activity POI Lists
        while (itMaster.hasNext()) {
            POI poi = itMaster.next();
            if (!containsPoiId(poi.id, l2)) {
                //If POI id isn't in Activity array remove from JME array
                poi.graphNode.removeFromParent();
                itMaster.remove();
            }
        }

        Iterator<POI> itReference = l2.iterator();
        while (itReference.hasNext()) {
            POI poi = itReference.next();
            if (!containsPoiId(poi.id, mPOIListJME)) {
                //If POI id isn't in JME array add it
                mPOIListJME.add(poi);
            }

        }
        //Control flow continues in setUserLocation

        return mPOIListJME;

    }

    /**
     * Checks for a POI id within a list of POI objects
     * @param id
     *          The id of the desired POI object
     * @param list
     *          List of POI objects
     * @return
     *          true if list contains id, false if not
     */
    private boolean containsPoiId(String id, List<POI> list){
        List<POI> cp = new LinkedList<>(list);
        for(POI poi:cp){
            if(poi.id.equals(id.toString())){
                return true;
            }
        }
        return false;
    }

    /**
     * Creates Spatial objects for new POIs and adds them to the root node.
     */
    private void loadPOIModels(){
        for (POI poi : mPOIListJME) {
            if (poi.graphNode == null) {
                try {
                    //Loads model for path given by poi object
                    poi.graphNode = assetManager.loadModel("Models/" + poi.filePath);
                    //Pre-sets scale
                    poi.graphNode.scale(poi.scale);
                    //Pre-sets rotation
                    poi.graphNode.rotate(0.0f, -3.0f, 0.0f);
                    //Pre-sets position
                    poi.graphNode.setLocalTranslation(0.0f, -2.5f, -50.0f);

                    /*poi.animControl = poi.graphNode.getControl(AnimControl.class);
                    poi.animControl.addListener(this);
                    poi.animChannel = poi.animControl.createChannel();
                    // show animation from beginning
                    poi.animChannel.setAnim("Walk");
                    poi.animChannel.setLoopMode(LoopMode.Loop);
                    poi.animChannel.setSpeed(1f);*/

                    rootNode.attachChild(poi.graphNode);

                }catch(AssetNotFoundException e){
                    Log.d(TAG,"AssetNotFoundException: "+poi.filePath,e);
                }
            }
        }
    }

    /**
     * Updates POI translations within the AR scenegraph using their ENU coordinates
     */
    private void setPOILocalTranslations(){
        //float i = 0f;
        for (POI poi : mPOIListJME) {
            //i += 3f;
            if(poi.enuLocation != null){
                /* For comparison
                 poi.graphNode.setLocalTranslation( i,
                        mNinjaPosition.y - 1.0f, mNinjaPosition.z + 0.0f);

                  poi.enuLocation.set(ENUpoi.x, 0, ENUpoi.y);
                  //x=east,y=north,z=up*/

                poi.graphNode.setLocalTranslation(poi.enuLocation.x + 0.f,
                        poi.enuLocation.y - 1.f, poi.enuLocation.z + 0.0f);

            }
        }
    }

    /**
     * Called at intervals as part of the main update thread to make changes to the
     * scenegraph
     * @param tpf
     *          Time per frame
     */
    @Override
    public void simpleUpdate(float tpf) {
        if (mNewCameraFrameAvailable) {
            mCameraTexture.setImage(mCameraImage);
            mvideoBGMat.setTexture("ColorMap", mCameraTexture);
        }

        if (mNewUserPositionAvailable) {
            Log.d(TAG, "update user location");
            ninja.setLocalTranslation(/*mNinjaPosition.x+0.0f*/ 0,
                    mNinjaPosition.y - 2.5f, mNinjaPosition.z + 0.0f);
            //Loads POI Models
            loadPOIModels();
            setPOILocalTranslations();
            mNewUserPositionAvailable=false;
        }

        if (mNewUserRotationFusedAvailable) {
            fgCam.setAxes(mCurrentCamRotationFused);
            mNewUserRotationFusedAvailable=false;
        }

        mVideoBGGeom.updateLogicalState(tpf);
        mVideoBGGeom.updateGeometricState();
    }

    @Override
    public void simpleRender(RenderManager rm) {
        // unused
    }

    /**
     * Converts from WSG84 to the ECEF coordinate format
     * @param loc
     *          Location in WSG84 format
     * @param position
     *          Position in ECEF format
     */
    private void WSG84toECEF(Location loc, Vector3f position) {

        double WGS84_A=6378137.0;           // WGS 84 semi-major axis constant in meters

        double WGS84_E=0.081819190842622;   // WGS 84 eccentricity

        double lat=(float) Math.toRadians(loc.getLatitude());
        double lon=(float) Math.toRadians(loc.getLongitude());

        double clat=Math.cos(lat);
        double slat=Math.sin(lat);
        double clon=Math.cos(lon);
        double slon=Math.sin(lon);

        double N=WGS84_A / Math.sqrt(1.0 - WGS84_E * WGS84_E * slat * slat);

        double x = N*clat*clon;
        double y = N*clat*slon;
        double z = (N * (1.0 - WGS84_E * WGS84_E)) * slat;

        position.set((float) x, (float) y, (float) z);
    }

    /**
     * Converts POI location from ECEF to the ENU coordinate format
     * @param loc
     *          User location lat/long
     * @param cameraPosition
     *          Camera position
     * @param poiPosition
     *          poiPosition
     * @param enuPOIPosition
     *          Variable to assign ENU POI location
     */
    private void ECEFtoENU(Location loc, Vector3f cameraPosition, Vector3f poiPosition, Vector3f enuPOIPosition) {
        double lat=(float) Math.toRadians(loc.getLatitude());
        double lon=(float) Math.toRadians(loc.getLongitude());

        double clat=Math.cos(lat);
        double slat=Math.sin(lat);
        double clon=Math.cos(lon);
        double slon=Math.sin(lon);

        double dx = cameraPosition.x - poiPosition.x;

        double dy = cameraPosition.y - poiPosition.y;

        double dz = cameraPosition.z - poiPosition.z;

        double e = -slon*dx  + clon*dy;

        double n = -slat*clon*dx - slat*slon*dy + clat*dz;

        double u = clat*clon*dx + clat*slon*dy + slat*dz;

        enuPOIPosition.set((float)e,(float)n,(float)u);
    }
}

