package com.openarbrowser.westernfrontar;

import android.provider.BaseColumns;

/**
 * Contract class defining the database schema and relevant queries.
 */
public final class POIDBContract {
    //http://developer.android.com/training/basics/data-storage/databases.html
    //http://stackoverflow.com/questions/17451931/how-to-use-a-contract-class-in-android
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public POIDBContract() {}

    /**
     *  Defines the fields in POITable
     */
    public static abstract class POITable implements BaseColumns {
        public static final String TABLE_NAME = "poi";
        public static final String COLUMN_NAME_POI_ID = "poiid";
        public static final String COLUMN_NAME_LATITUDE = "lat";
        public static final String COLUMN_NAME_LONGITUDE = "long";
        public static final String COLUMN_NAME_MODEL = "model";
        public static final String COLUMN_NAME_SCALE = "scale";

        public static final String TEXT_TYPE = " TEXT";
        public static final String REAL_TYPE = " REAL";
        public static final String COMMA_SEP = ",";

        //Query to create the table
        public static final String CREATE_POI_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        COLUMN_NAME_POI_ID + TEXT_TYPE + COMMA_SEP +
                        COLUMN_NAME_LATITUDE + REAL_TYPE + COMMA_SEP +
                        COLUMN_NAME_LONGITUDE + REAL_TYPE + COMMA_SEP +
                        COLUMN_NAME_MODEL + TEXT_TYPE + COMMA_SEP +
                        COLUMN_NAME_SCALE + REAL_TYPE +
                        ");";

        //Query to delete the table
        public static final String DELETE_POI_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

        //Query to delete the table
        public static final String CREATE_TEMPORARY_POI_TABLE =
                "ALTER TABLE " + TABLE_NAME + " RENAME TO temp;";

        //Query to delete the table
        public static final String DELETE_TEMP =
                "DROP TABLE IF EXISTS temp;";

        //Query to delete the table
        public static final String REPOPULATE_UPDATED_POI_TABLE =
                "INSERT INTO" + TABLE_NAME + "(" +
                        COLUMN_NAME_POI_ID + TEXT_TYPE + COMMA_SEP +
                        COLUMN_NAME_LATITUDE + REAL_TYPE + COMMA_SEP +
                        COLUMN_NAME_LONGITUDE + REAL_TYPE + COMMA_SEP +
                        COLUMN_NAME_MODEL + TEXT_TYPE + COMMA_SEP +
                        ") SELECT +" +
                        COLUMN_NAME_POI_ID + COMMA_SEP +
                        COLUMN_NAME_LATITUDE + COMMA_SEP +
                        COLUMN_NAME_LONGITUDE + COMMA_SEP +
                        COLUMN_NAME_MODEL + " FROM temp;";
    }

    public static abstract class android_metadata implements BaseColumns {
        public static final String TABLE_NAME = "android_metadata";
        public static final String LOCALE = "locale";

        public static final String TEXT_TYPE = " TEXT";

        //Query to create the table
        public static final String CREATE_ANDROID_METADATA_TABLE =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + LOCALE + TEXT_TYPE + ");";

        public static final String DELETE_ANDROID_METADATA_TABLE =
                "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }

}
