package com.openarbrowser.westernfrontar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PointF;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Processes device location and passes it to the container Activity via a callback
 *
 * Based on LocationAccessJME from Chapter 4.
 *
 * Jens Grubert, Raphael Grasset "Augmented Reality for Android Application Development", Packt Publishing, 2013.
 */
public class LocationAccessFragment extends Fragment {

    private static final String TAG = "LocationFrag";

    private static OnLocationEventListener mLocationListener;

    private LocationManager locationManager;
    private static Location mLocation;

    // the following variables are relevant for the
    // section "Getting content for you AR browser - the Google Place API"

    private boolean mUseGooglePlaces = false; // make sure to set this flag to true if you want to use Google Places
    private boolean mUsePOIDb = true;
    private static List<POI> mGooglePOIs;
    private static List<POI> mDbPOIs;
    private HttpClient mHttpClient;
    private static int mPlacesRadius = 2000;
    private String mPlacesKey = "AIzaSyBi_ISLfjrT3mjKxAMrNq-AGM7A1qPnhg0";// "<YOUR API KEY HERE>";

    private static SQLiteDatabase mReadablePOIDatabase;

    private static OpenReadablePOIDatabaseAsyncTask openDbTask;

    private static boolean devMode = true;

    /**
     * Class used to open a readable database. Extends AsyncTask.
     */
    private class OpenReadablePOIDatabaseAsyncTask extends AsyncTask<POIDBHelper,Void,SQLiteDatabase>{
        //http://developer.android.com/reference/android/os/AsyncTask.html
        @Override
        protected SQLiteDatabase doInBackground(POIDBHelper... params) {
            SQLiteDatabase poiDb = params[0].getReadableDatabase();

            return poiDb;
        }

        @Override
        protected void onPostExecute(SQLiteDatabase sqLiteDatabase) {
            super.onPostExecute(sqLiteDatabase);
            mReadablePOIDatabase = sqLiteDatabase;
        }
    }

    private class QueryPOIDatabaseAsyncTask extends AsyncTask<Location,Void,List<POI>>{
        //http://developer.android.com/reference/android/os/AsyncTask.html
        @Override
        protected List doInBackground(Location... params) {

            List<POI> dBPOIList = new LinkedList<>();
            //send query
            //http://developer.android.com/training/basics/data-storage/databases.html
            // Define a projection that specifies which columns from the database
            // you will actually use after this query.
            String[] projection = {
                    POIDBContract.POITable._ID,
                    POIDBContract.POITable.COLUMN_NAME_POI_ID,
                    POIDBContract.POITable.COLUMN_NAME_LATITUDE,
                    POIDBContract.POITable.COLUMN_NAME_LONGITUDE,
                    POIDBContract.POITable.COLUMN_NAME_MODEL,
                    POIDBContract.POITable.COLUMN_NAME_SCALE
            };

            //query radius around gps location


            //http://www.movable-type.co.uk/scripts/latlong.html
            //http://stackoverflow.com/questions/3695224/sqlite-getting-nearest-locations-with-latitude-and-longitude
            Location location = new Location(mLocation);
            float x = new Double(location.getLatitude()).floatValue();
            float y = new Double(location.getLongitude()).floatValue();

            PointF centre = new PointF(x, y);
            final double mult = 1.1; // mult = 1.1; is more reliable
            PointF p1 = calculateDerivedPosition(centre, mult * mPlacesRadius, 0);
            PointF p2 = calculateDerivedPosition(centre, mult * mPlacesRadius, 90);
            PointF p3 = calculateDerivedPosition(centre, mult * mPlacesRadius, 180);
            PointF p4 = calculateDerivedPosition(centre, mult * mPlacesRadius, 270);

            String selection =
                    POIDBContract.POITable.COLUMN_NAME_LATITUDE + " < " + String.valueOf(p1.x) + " AND "
                            + POIDBContract.POITable.COLUMN_NAME_LATITUDE + " > " + String.valueOf(p3.x) + " AND "
                            + POIDBContract.POITable.COLUMN_NAME_LONGITUDE + " > " + String.valueOf(p4.y) + " AND "
                            + POIDBContract.POITable.COLUMN_NAME_LONGITUDE + " < " + String.valueOf(p2.y);

            // How you want the results sorted in the resulting Cursor
            String sortOrder =
                    POIDBContract.POITable.COLUMN_NAME_LATITUDE + " DESC";
            Cursor c = null;

            try {
                c = mReadablePOIDatabase.query(
                        POIDBContract.POITable.TABLE_NAME,  // The table to query
                        projection,                               // The columns to return
                        selection,                                // The columns for the WHERE clause
                        null,                            // The values for the WHERE clause
                        null,                                     // don't group the rows
                        null,                                     // don't filter by row groups
                        sortOrder                                 // The sort order
                );

                c.moveToFirst();
                while (!c.isAfterLast()) {
                    POI poi = new POI();

                    try {
                        //Set POI id
                        poi.id = c.getString(
                                c.getColumnIndexOrThrow(POIDBContract.POITable.COLUMN_NAME_POI_ID));
                    } catch (Exception e) {
                        Log.d(TAG, "No POI id returned in query");
                    }

                    try {
                        //File path must be a string to be loaded by JME AssetLoader
                        poi.filePath = c.getString(c.getColumnIndexOrThrow(POIDBContract.POITable.COLUMN_NAME_MODEL));
                        Log.d(TAG, "poi.filePath: " + poi.filePath);
                    } catch (Exception e) {
                        Log.d(TAG, "No file path returned in query",e);
                    }

                    try{
                        //Sets the model scale
                        poi.scale = c.getFloat(c.getColumnIndexOrThrow(POIDBContract.POITable.COLUMN_NAME_SCALE));
                        Log.d(TAG, "poi.scale: " + poi.scale);
                    }catch (Exception e){
                        Log.d(TAG, "No model scale returned in query",e);

                    }

                    //Set POI location
                    PointF point = new PointF();
                    try {
                        point.x = c.getFloat(
                                c.getColumnIndexOrThrow(POIDBContract.POITable.COLUMN_NAME_LATITUDE));
                        point.y = c.getFloat(
                                c.getColumnIndexOrThrow(POIDBContract.POITable.COLUMN_NAME_LONGITUDE));
                        Log.d(TAG, "point x: " + point.x + " point y: " + point.y);
                    } catch (Exception e) {
                        Log.d(TAG, "No location returned in query");
                    }

                    Log.d(TAG, "pointIsInCircle: " + Boolean.toString(pointIsInCircle(point, centre, mPlacesRadius)));

                    if (pointIsInCircle(point, centre, mPlacesRadius) && poi.filePath != null) {
                        Location loc = new Location(LocationManager.GPS_PROVIDER);
                        loc.setLatitude(point.x);
                        loc.setLongitude(point.y);
                        poi.gpsLocation = loc;

                        Log.d(TAG, "Before .add(): " + dBPOIList.toString());

                        dBPOIList.add(poi);

                        Log.d(TAG, "After .add()" + dBPOIList.toString());
                    }

                    c.moveToNext();
                }

            } finally {
                //closes the cursor even if an exception is thrown
                if (c != null)
                    c.close();
                return dBPOIList;
            }
        }

        @Override
        protected void onPostExecute(List<POI> dBPOIList){
            super.onPostExecute(dBPOIList);
            mLocationListener.onPOIQueryHandled(dBPOIList);
        }
    }

    private LocationListener locListener= new LocationListener() {

        private static final String TAG = "LocationListener";

        @Override
        public void onLocationChanged(final Location location) {
            Log.d(TAG, "onLocationChanged: " + location.toString());
            mLocation = location;
            mLocationListener.onUserLocationProcessed((Location) mLocation);
            new QueryPOIDatabaseAsyncTask().execute(location);

        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d(TAG, "onStatusChanged: " + status);
        }

    };

    /**
     *This looks to be inactive, and its functionality should be moved to locListener. Unclear until testing starts

    public Handler locationUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                if (mLocation == null) {
                    try {
                        if (locationManager != null) {
                            mLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        }
                        Log.d(TAG, "Setting initial location: " + mLocation.toString());
                        //Triggers LocationAccessFragment's update method, passing it the updated fusedOrientation array
                        mLocationListener.onUserLocationProcessed((Location) mLocation);

                    } catch (Exception e){
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplication());
                        //Chain together various setter methods to set the dialog characteristics
                        builder.setMessage("Could not query last known location").setTitle("GPS Error");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // do nothing
                            }
                        });
                        // Get the AlertDialog from create()
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }

                if(mLocation != null && mUsePOIDb == true) {
                    sendPOIDbQuery(mLocation, placesPOIQuerySQLiteHandler);

                }

                if(mLocation != null && mUseGooglePlaces == true) {
                    HttpParams params = new BasicHttpParams();
                    HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
                    HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
                    HttpProtocolParams.setUseExpectContinue(params, true);
                    SchemeRegistry schReg = new SchemeRegistry();
                    schReg.register(new Scheme("http",
                            PlainSocketFactory.getSocketFactory(), 80));
                    schReg.register(new Scheme("https",
                            SSLSocketFactory.getSocketFactory(), 443));
                    ClientConnectionManager conMgr = new
                            ThreadSafeClientConnManager(params,schReg);
                    mHttpClient = new DefaultHttpClient(conMgr, params);

                    try {
                        sendGooglePlacesQuery(mLocation, placesPOIQueryJSONHandler);
                    } catch (Exception e) {
                        //Must getActivity() as requires context
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplication());
                        //Chain together various setter methods to set the dialog characteristics
                        builder.setMessage("Error in sending the Google Places query.").setTitle("Google Places Error");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // do nothing
                            }
                        });
                        // Get the AlertDialog from create()
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            } catch(Exception e) {
                Log.e(TAG, e.toString());
            }
        }
    };*/


    public static Handler placesPOIQueryJSONHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                JSONObject response = new JSONObject(msg.obj.toString());
                JSONArray results = response.getJSONArray("results");
                Log.d(TAG, "results has length: " + results.length());
                for(int i = 0; i < results.length(); ++i) {
                    JSONObject curResult = results.getJSONObject(i);
                    String poiName = curResult.getString("id");
                    String poiReference = curResult.getString("reference");
                    double lat = curResult.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                    double lng = curResult.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
                    Log.d(TAG, "poiReference: " + poiReference);
                    Log.d(TAG, "poiName, lat, long: " + poiName +  " " +  lat + " " + lng);

                    Location refLoc = new Location(LocationManager.GPS_PROVIDER);
                    refLoc.setLatitude(lat);
                    refLoc.setLongitude(lng);

                    mGooglePOIs.add(new POI(poiReference, poiName, refLoc));
                }
                //Pass full mGooglePOIs array to onPOIQueryHandled callback in Activity
                mLocationListener.onPOIQueryHandled(mGooglePOIs);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }
    };

    /**
     * Interface must be implemented by container Activity. Defines methods
     * used to pass location data to the Activity using callbacks.
     */
    public interface OnLocationEventListener {
        public void onUserLocationProcessed(Location mLocation);
        public void onPOIQueryHandled(List<POI> poiList);
    }

    @Override
    /**
     * Creates a callback between the Fragment and Activity using the OnLocationEventListener
     * interface. Also opens a readable database.
     */
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        //http://developer.android.com/guide/components/fragments.html#Lifecycle
        super.onAttach(activity);
        try {
            mLocationListener = (OnLocationEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnUserLocationProcessedListener");
        }

        if (devMode)
        {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());
        }

        //getActivity().deleteDatabase("POI.db");

        //Android database documentation recommends using an AsyncTask for opening the database
        POIDBHelper poiDbHelper = new POIDBHelper(getActivity().getApplicationContext());

        //Before opening the database, copy it.
        try{
            copyDataBase(POIDBHelper.DATABASE_NAME);
        }catch(Exception e){
            Log.d(TAG,"Database not successfully copied. Exception: " + e.toString());
        }

        openDbTask = new OpenReadablePOIDatabaseAsyncTask();
        //Call the AsyncTask to open the readable database
        openDbTask.execute(poiDbHelper);


        if (mLocation != null && mUsePOIDb == true) {
            try {
                new QueryPOIDatabaseAsyncTask().execute(mLocation);
            } catch (Exception e) {
                Log.d(TAG, "Db Query Failed: " + e.toString());
            }
        }
    }

    @Override
    /**
     * Gets the location manager system service. Finds the user's current location.
     * Initialises the HttpParams and SchemeRegistry classes before calling sendGooglePlacesQuery().
     */
    public void onResume() {
        super.onResume();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, locListener);

        //If location not known get location, else if get location fails launch AlertDialog notification
        if (mLocation == null) {
            try {
                if (locationManager != null) {
                    mLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                Log.d(TAG, "Setting initial location: " + mLocation.toString());
                //Triggers LocationAccessFragment's update method, passing it the updated fusedOrientation array
                mLocationListener.onUserLocationProcessed(mLocation);

            } catch (Exception e) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getWindow().getContext());
                //Have replaced 'this' with a call to the application context
                //AlertDialog.Builder builder = new AlertDialog.Builder(this);
                //Chain together various setter methods to set the dialog characteristics
                builder.setMessage("Please make sure you enabled your GPS sensor and already retrieved an initial position.").setTitle("GPS Error");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });
                // Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();

            }

        }
    }

    public void onPause(){
        super.onPause();
        //dbQueryThread.pause();
    }


        //Initialise the HttpParams and SchemeRegistry classes used by sendGooglePlacesQuery.
        /*if(mLocation != null && mUseGooglePlaces == true) {
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
            HttpProtocolParams.setUseExpectContinue(params, true);
            SchemeRegistry schReg = new SchemeRegistry();
            schReg.register(new Scheme("http",
                    PlainSocketFactory.getSocketFactory(), 80));
            schReg.register(new Scheme("https",
                    SSLSocketFactory.getSocketFactory(), 443));
            ClientConnectionManager conMgr = new
                    ThreadSafeClientConnManager(params,schReg);
            mHttpClient = new DefaultHttpClient(conMgr, params);

            //Call sendGooglePlacesQuery. If it throws an exception launch AlertDialog notification
            try {
                sendGooglePlacesQuery(mLocation, placesPOIQueryJSONHandler);
            } catch (Exception e) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());
                //Have replaced 'this' with a call to the application context
                //AlertDialog.Builder builder = new AlertDialog.Builder(this);
                //Chain together various setter methods to set the dialog characteristics
                builder.setMessage("Error in sending the Google Places query.").setTitle("Google Places Error");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                });
                // Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }*/


    /**
     * Copies your database from your local assets-folder to the just created
     * empty database in the system folder, from where it can be accessed and
     * handled. This is done by transferring byte-stream.
     *
     * Source: http://stackoverflow.com/questions/10738623/copy-database-from-assets-folder-in-unrooted-device
     * */
    private void copyDataBase(String dbname) throws IOException {
        // Open your local db as the input stream
        InputStream myInput = getActivity().getAssets().open(dbname);
        // Path to the just created empty db
        String outFileName = getActivity().getDatabasePath(dbname).getPath();
        // Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }


    /**
     * Queries GooglePlaces for POIs within mPlacesRadius distance of the user's
     * current location. Results from the query are assigned to a message and passed to the
     * Thread's handler.
     *
     * @param location
     *              User's current location
     * @param guiHandler
     *              The handler used to send messages out of the thread
     * @throws Exception
     */
    public void sendGooglePlacesQuery(final Location location, final Handler guiHandler) throws Exception  {
        Thread t = new Thread() {
            public void run() {
                Looper.prepare(); //For Preparing Message Pool for the child Thread
                BufferedReader in = null;
                try {
                    String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + location.getLatitude() + "," + location.getLongitude() + "&radius=" +  mPlacesRadius + "&sensor=true&key=" + mPlacesKey;
                    HttpConnectionParams.setConnectionTimeout(mHttpClient.getParams(), 10000); //Timeout Limit
                    HttpResponse response;
                    Log.i(TAG, "Sending GET request: " + url);
                    HttpGet get = new HttpGet(url);
                    response = mHttpClient.execute(get);
                    Log.i(TAG, "Processing response");
                    Message toGUI = guiHandler.obtainMessage();
                    if(response == null)
                    {
                        toGUI.obj = "";
                    } else
                    {
                        in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                        StringBuffer sb = new StringBuffer("");
                        String line = "";
                        String NL = System.getProperty("line.separator");
                        while ((line = in.readLine()) != null) {
                            sb.append(line + NL);
                        }
                        in.close();
                        String result = sb.toString();
                        toGUI.obj = result;
                        guiHandler.sendMessage(toGUI);
                    }
                    //return result;
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }

                finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                }
                Looper.loop(); //Loop in the message queue
            } // end run
        }; // end thread
        t.start();
    }


    /**
     * Calculates the end-point from a given source at a given range (meters)
     * and bearing (degrees). This methods uses simple geometry equations to
     * calculate the end-point.
     *
     * Source: http://stackoverflow.com/questions/3695224/sqlite-getting-nearest-locations-with-latitude-and-longitude
     *
     * @param point
     *            Point of origin
     * @param range
     *            Range in meters
     * @param bearing
     *            Bearing in degrees
     * @return End-point from the source given the desired range and bearing.
     */
    private static PointF calculateDerivedPosition(PointF point,
                                                  double range, double bearing)
    {
        double EarthRadius = 6371000; // m

        double latA = Math.toRadians(point.x);
        double lonA = Math.toRadians(point.y);
        double angularDistance = range / EarthRadius;
        double trueCourse = Math.toRadians(bearing);

        double lat = Math.asin(
                Math.sin(latA) * Math.cos(angularDistance) +
                        Math.cos(latA) * Math.sin(angularDistance)
                                * Math.cos(trueCourse));

        double dlon = Math.atan2(
                Math.sin(trueCourse) * Math.sin(angularDistance)
                        * Math.cos(latA),
                Math.cos(angularDistance) - Math.sin(latA) * Math.sin(lat));

        double lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

        lat = Math.toDegrees(lat);
        lon = Math.toDegrees(lon);

        PointF newPoint = new PointF((float) lat, (float) lon);

        return newPoint;

    }

    /**
     * Checks if a point is within a circle
     *
     * Sources: http://stackoverflow.com/questions/3695224/sqlite-getting-nearest-locations-with-latitude-and-longitude
     *          http://www.movable-type.co.uk/scripts/latlong.html
     *
     * @param pointForCheck
     * @param center
     * @param radius
     * @return
     */
    private static boolean pointIsInCircle(PointF pointForCheck, PointF center,
                                          double radius) {
        if (getDistanceBetweenTwoPoints(pointForCheck, center) <= radius)
            return true;
        else
            return false;
    }

    /**
     * Finds the distance between two points
     *
     * Sources: http://stackoverflow.com/questions/3695224/sqlite-getting-nearest-locations-with-latitude-and-longitude
     *          http://www.movable-type.co.uk/scripts/latlong.html
     *
     * @param p1
     * @param p2
     * @return
     */
    private static double getDistanceBetweenTwoPoints(PointF p1, PointF p2) {
        double R = 6371000; // m
        double dLat = Math.toRadians(p2.x - p1.x);
        double dLon = Math.toRadians(p2.y - p1.y);
        double lat1 = Math.toRadians(p1.x);
        double lat2 = Math.toRadians(p2.x);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2)
                * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;

        return d;
    }
}
