
/* LocationAccessJMEActivity - LocationAccessJME Example
 *
 * Example Chapter 4
 * accompanying the book
 * "Augmented Reality for Android Application Development", Packt Publishing, 2013.
 *
 * Copyright © 2013 Jens Grubert, Raphael Grasset / Packt Publishing.
 *
 */

package com.openarbrowser.westernfrontar;

import com.jme3.app.AndroidHarness;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import java.nio.ByteBuffer;
import java.util.List;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;

//include packages for Android Location API
import android.location.Location;

//import com.jme3.system.android.AndroidConfigChooser;
import com.jme3.system.android.AndroidConfigChooser.ConfigType;
import com.jme3.texture.Image;

public class WesternFrontARActivity extends AndroidHarness implements LocationAccessFragment.OnLocationEventListener,SensorAccessFragment.OnOrientationProcessedListener {

    private static final String TAG = "WesternFrontARAct";
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private Camera mCamera;
    private CameraPreview mPreview;
    private int mDesiredCameraPreviewWidth = 640;
    private byte[] mPreviewBufferRGB565 = null;
    java.nio.ByteBuffer mPreviewByteBufferRGB565;
    // the actual size of the preview images
    int mPreviewWidth;
    int mPreviewHeight;
    // If we have to convert the camera preview image into RGB565 or can use it
    // directly
    private boolean pixelFormatConversionNeeded = true;

    private boolean stopPreview = false;
    Image cameraJMEImageRGB565;


    // Implement the interface for getting copies of preview frames
    private final Camera.PreviewCallback mCameraCallback = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera c) {
            if (c != null && stopPreview == false) {
                mPreviewByteBufferRGB565.clear();
                // Perform processing on the camera preview data.
                if (pixelFormatConversionNeeded) {
                    yCbCrToRGB565(data, mPreviewWidth, mPreviewHeight,
                            mPreviewBufferRGB565);
                    mPreviewByteBufferRGB565.put(mPreviewBufferRGB565);
                } else {
                    mPreviewByteBufferRGB565.put(data);
                }
                cameraJMEImageRGB565.setData(mPreviewByteBufferRGB565);
                if ((com.openarbrowser.westernfrontar.WesternFrontARJME) app != null) {
                    ((com.openarbrowser.westernfrontar.WesternFrontARJME) app)
                            .setVideoBGTexture(cameraJMEImageRGB565);
                }
            }
        }
    };

    public void onOrientationProcessed(float[] fusedOrientation){
        Log.d(TAG,"onOrientationProcessed");
        if ((com.openarbrowser.westernfrontar.WesternFrontARJME) app != null) {
            ((com.openarbrowser.westernfrontar.WesternFrontARJME) app).setRotationFused((float)(fusedOrientation[2]), (float)(-fusedOrientation[0]), (float)(fusedOrientation[1]));
        }
    }

    public void onUserLocationProcessed(Location location){
        Log.d(TAG,"onLocationProcessed");
        if ((com.openarbrowser.westernfrontar.WesternFrontARJME) app != null) {
            ((com.openarbrowser.westernfrontar.WesternFrontARJME) app).setUserLocation(location);
        }
    }

    public void onPOIQueryHandled(List<POI> poiList) {
        Log.d(TAG,"onPOIQueryHandled");
        //Send POIs to JME application
        if ((com.openarbrowser.westernfrontar.WesternFrontARJME) app != null) {
            ((com.openarbrowser.westernfrontar.WesternFrontARJME) app).setPOIList(poiList);
        }
    }

    public WesternFrontARActivity() {
        // Set the application class to run
        appClass = "com.openarbrowser.westernfrontar.WesternFrontARJME";
        // Try ConfigType.FASTEST; or ConfigType.LEGACY if you have problems
        eglConfigType = ConfigType.BEST;
        // Exit Dialog title & message
        exitDialogTitle = "Exit?";
        exitDialogMessage = "Press Yes";
        // Enable verbose logging
        eglConfigVerboseLogging = true;
        // Choose screen orientation
        screenOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        mouseEventsInvertX = true;
        // Invert the MouseEvents Y (default = true)
        mouseEventsInvertY = true;

    }

    /**
     * Overrides AndroidHarness.onCreate() to load the needed Fragment classes.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Create FragmentManager
        mFragmentManager = getFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(new SensorAccessFragment(),"SensorFrag");
        mFragmentTransaction.addToBackStack("SensorFrag");
        mFragmentTransaction.add(new LocationAccessFragment(), "LocationFrag");
        mFragmentTransaction.addToBackStack("LocationFrag");
        mFragmentTransaction.commit();
    }

    /**
     * Overrides AndroidHarness.onResume() to create the SurfaceView required for
     * the camera preview.
     */
    @Override
    public void onResume() {
        super.onResume();
        stopPreview = false;
        //Sets the SurfaceView debug flags to verbose
        view.setDebugFlags(GLSurfaceView.DEBUG_LOG_GL_CALLS);
        // make sure the AndroidGLSurfaceView view is on top of the view
        // hierarchy
        view.setZOrderOnTop(true);
        // Create an instance of Camera
        mCamera = getCameraInstance();
        // initialize camera parameters
        initializeCameraParameters();

        // register our callback function to get access to the camera preview
        // frames
        preparePreviewCallbackBuffer();

        if (mCamera == null) {
            Log.e(TAG, "Camera not available");
        } else {
            // Create our Preview view and set it as the content of our
            // activity.
            mPreview = new CameraPreview(this, mCamera, mCameraCallback);
            // We do not want to display the Camera Preview view at startup - so
            // we resize it to 1x1 pixel.
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(1, 1);
            addContentView(mPreview, lp);
        }
    }

    /**
     * Overrides the AndroidHarness.onPause() method to release the
     * Camera preview.
     */
    @Override
    protected void onPause() {
        stopPreview = true;
        super.onPause();
        // Make sure to release the camera immediately on pause.
        releaseCamera();
        // remove the SurfaceView
        ViewGroup parent = (ViewGroup) mPreview.getParent();
        parent.removeView(mPreview);
    }

    /**
     * Overrides the onDestroy() method to stop JME application.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ((com.openarbrowser.westernfrontar.WesternFrontARJME) app != null) {
            ((com.openarbrowser.westernfrontar.WesternFrontARJME) app).stop();
        }
    }

    /**
     * Releases the Camera instance.
     */
    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            // Release the camera.
            mCamera.release();
            mCamera = null;
        }
    }

    /**
     * Retrieve an instance of the Camera object.
     * @return
     *         Camera instance
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            // get a Camera instance
            c = Camera.open(0);
        } catch (Exception e) {
            // Camera is does not exist or is already in use
            Log.e(TAG, "Camera not available or in use.");
        }
        // return NULL if camera is unavailable, otherwise return the Camera
        // instance
        return c;
    }

    /**
     * Configure camera parameters like preview siz
     */
    private void initializeCameraParameters() {
        Camera.Parameters parameters = mCamera.getParameters();
        // Get a list of supported preview sizes.
        List<Camera.Size> sizes = mCamera.getParameters()
                .getSupportedPreviewSizes();
        int currentWidth = 0;
        int currentHeight = 0;
        boolean foundDesiredWidth = false;
        for (Camera.Size s : sizes) {
            if (s.width == mDesiredCameraPreviewWidth) {
                currentWidth = s.width;
                currentHeight = s.height;
                foundDesiredWidth = true;
                break;
            }
        }
        if (foundDesiredWidth) {
            parameters.setPreviewSize(currentWidth, currentHeight);
        }
        // we also want to use RGB565 directly
        List<Integer> pixelFormats = parameters.getSupportedPreviewFormats();
        for (Integer format : pixelFormats) {
            if (format == ImageFormat.RGB_565) {
                Log.d(TAG, "Camera supports RGB_565");
                pixelFormatConversionNeeded = false;
                parameters.setPreviewFormat(format);
                break;
            }
        }
        if (pixelFormatConversionNeeded == true) {
            Log.e(TAG,
                    "Camera does not support RGB565 directly. Need conversion");
        }
        mCamera.setParameters(parameters);
    }

    /**
     * Prepares the Camera preview callback buffers.
     */
    public void preparePreviewCallbackBuffer() {
        int pformat;
        pformat = mCamera.getParameters().getPreviewFormat();
        Log.e(TAG, "PREVIEW format: " + pformat);
        // Get pixel format information to compute buffer size.
        PixelFormat info = new PixelFormat();
        PixelFormat.getPixelFormatInfo(pformat, info);
        // The actual preview width and height.
        // They can differ from the requested width mDesiredCameraPreviewWidth
        mPreviewWidth = mCamera.getParameters().getPreviewSize().width;
        mPreviewHeight = mCamera.getParameters().getPreviewSize().height;
        int bufferSizeRGB565 = mPreviewWidth * mPreviewHeight * 2 + 4096;
        //Delete buffer before creating a new one.
        mPreviewBufferRGB565 = null;
        mPreviewBufferRGB565 = new byte[bufferSizeRGB565];
        mPreviewByteBufferRGB565 = ByteBuffer.allocateDirect(mPreviewBufferRGB565.length);
        cameraJMEImageRGB565 = new Image(Image.Format.RGB565, mPreviewWidth,
                mPreviewHeight, mPreviewByteBufferRGB565);
    }

    /**
     * Converts images from the yCbCr to the RGB565 format.
     * @param yuvs
     * @param width
     * @param height
     * @param rgbs
     */
    public static void yCbCrToRGB565(byte[] yuvs, int width, int height,
                                     byte[] rgbs) {

        // the end of the luminance data
        final int lumEnd = width * height;
        // points to the next luminance value pair
        int lumPtr = 0;
        // points to the next chromiance value pair
        int chrPtr = lumEnd;
        // points to the next byte output pair of RGB565 value
        int outPtr = 0;
        // the end of the current luminance scanline
        int lineEnd = width;

        while (true) {

            // skip back to the start of the chromiance values when necessary
            if (lumPtr == lineEnd) {
                if (lumPtr == lumEnd)
                    break; // we've reached the end
                // division here is a bit expensive, but's only done once per
                // scanline
                chrPtr = lumEnd + ((lumPtr >> 1) / width) * width;
                lineEnd += width;
            }

            // read the luminance and chromiance values
            final int Y1 = yuvs[lumPtr++] & 0xff;
            final int Y2 = yuvs[lumPtr++] & 0xff;
            final int Cr = (yuvs[chrPtr++] & 0xff) - 128;
            final int Cb = (yuvs[chrPtr++] & 0xff) - 128;
            int R, G, B;

            // generate first RGB components
            B = Y1 + ((454 * Cb) >> 8);
            if (B < 0)
                B = 0;
            else if (B > 255)
                B = 255;
            G = Y1 - ((88 * Cb + 183 * Cr) >> 8);
            if (G < 0)
                G = 0;
            else if (G > 255)
                G = 255;
            R = Y1 + ((359 * Cr) >> 8);
            if (R < 0)
                R = 0;
            else if (R > 255)
                R = 255;
            // NOTE: this assume little-endian encoding
            rgbs[outPtr++] = (byte) (((G & 0x3c) << 3) | (B >> 3));
            rgbs[outPtr++] = (byte) ((R & 0xf8) | (G >> 5));

            // generate second RGB components
            B = Y2 + ((454 * Cb) >> 8);
            if (B < 0)
                B = 0;
            else if (B > 255)
                B = 255;
            G = Y2 - ((88 * Cb + 183 * Cr) >> 8);
            if (G < 0)
                G = 0;
            else if (G > 255)
                G = 255;
            R = Y2 + ((359 * Cr) >> 8);
            if (R < 0)
                R = 0;
            else if (R > 255)
                R = 255;
            // NOTE: this assume little-endian encoding
            rgbs[outPtr++] = (byte) (((G & 0x3c) << 3) | (B >> 3));
            rgbs[outPtr++] = (byte) ((R & 0xf8) | (G >> 5));
        }
    }

}

