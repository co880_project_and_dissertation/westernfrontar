package com.openarbrowser.westernfrontar;

import android.location.Location;
import android.location.LocationManager;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

/**
 * Class defines fields that describe a Point Of Interest
 */
public class POI {
    public String placesReference;
    public String id;
    public Location gpsLocation;
    public Vector3f enuLocation;
    public Spatial graphNode;
    public AnimControl animControl;
    public AnimChannel animChannel;
    public String filePath;
    public float scale;
    public POI() {
        placesReference = "";
        id = "";
        gpsLocation = new Location(LocationManager.GPS_PROVIDER);
        enuLocation = new Vector3f();
        graphNode = null;
        animControl = null;
        animChannel = null;
        scale = 0.5f;
    }
    public POI(String ref, String n, Location loc) {
        placesReference = ref;
        id = n;
        gpsLocation = loc;
    }

}
