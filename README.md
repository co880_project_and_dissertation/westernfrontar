# README #

### Western Front AR ###

Western Front AR is an augmented reality field aid aimed at supporting military historians.

A demo video of the software is available by following the link to [this dropbox share](https://www.dropbox.com/sh/cpqfw3cqidt0e4i/AAA0vF9tdp1QGtKvYKhWFuLYa?dl=0)

### How do I get set up? ###

* Clone the repository 

* Open the project using Android Studio

* Compile the project

* Choose to run the application on physical or emulated hardware.